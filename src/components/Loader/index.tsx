import React from "react";

import { Spinner } from "./components/Spinner";

import "./style.css";

export function Loader({ className, phrase }: { className?: string; phrase: string }) {
  const classes = ["Loader", className].filter(Boolean).join(" ");

  return (
    <div className={classes}>
      <Spinner />
      <p data-test-hook="phrase" className="Loader__Phrase">
        {phrase}
      </p>
    </div>
  );
}
